/*
 * The library is currently only tested on little endian architectures.
 * Note that iOS and Android on Arm also operate on little endian.
 * Those checks are here, so that we can investigate compatibility in case
 * we stumble upon a big endian architecture.
 * */

// https://gist.github.com/hamsham/9849411

/*
 * A simple compile-time endian test
 * g++ -std=c++11 -Wall -Werror -Wextra -pedantic -pedantic-errors endian.cpp -o endian
 *
 * This can be used with specialized template functions, classes, and class
 * methods in order better tailor code and reduce reliance on runtime
 * checking systems.
 */

#include <cstdint>
#include <iostream>

/**
 * endian_t
 *
 * This enumeration can be placed into templated objects in order to generate
 * compile-time code based on a program's target endianness.
 *
 * The values placed in this enum are used just in case the need arises in
 * order to manually compare them against the number order in the
 * endianValues[] array.
 */
enum endian_t : uint32_t {
    GS_LITTLE_ENDIAN   = 0x00000001,
    GS_BIG_ENDIAN      = 0x01000000,
    GS_PDP_ENDIAN      = 0x00010000,
    GS_UNKNOWN_ENDIAN  = 0xFFFFFFFF
};

/**
 * A simple function that can be used to help determine a program's endianness
 * at compile-time.
 */
constexpr endian_t getEndianOrder() {
    return
            ((0xFFFFFFFF & 1) == GS_LITTLE_ENDIAN)
            ? GS_LITTLE_ENDIAN
            : ((0xFFFFFFFF & 1) == GS_BIG_ENDIAN)
              ? GS_BIG_ENDIAN
              : ((0xFFFFFFFF & 1) == GS_PDP_ENDIAN)
                ? GS_PDP_ENDIAN
                : GS_UNKNOWN_ENDIAN;
}

static_assert(getEndianOrder() == GS_LITTLE_ENDIAN, "machine is not little endian");
